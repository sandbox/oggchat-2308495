<?php

/**
 * @file
 * Administrative page callbacks for the OggChat.
 */

/**
 * Implementation of hook_admin_settings() for configuring the module
 *
 * @param array $form_state
 *   structure associative drupal form array
 * @return array
 */
function oggchat_admin_settings_form() {
  $form['oggchat'] = array(
    '#type' => 'vertical_tabs',
  );

  // General Settings
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => FALSE,
    '#group' => 'oggchat',
  );
  $form['account']['oggchat_account'] = array(
    '#type' => 'textfield',
    '#title' => t('OggChat Chat Widget Key'),
    '#default_value' => variable_get('oggchat_account', ''),
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => '<p>' . t('Your chat widget key can be found in the OggChat Dashboard.') . '</p>' .
                      '<p>' . t('Go to !url, login, and grab the chat key from your Dashboard once you complete the setup tasks.', array('!url' => l('oggchat.com', 'http://oggchat.com/', array('attibutes' => array('target' => '_blank'))))) . '</p>' .
                      t("", array('!code' => '')),
  );
  // END General Settings

  // Role specific visibility configurations.
  $form['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Roles'),
    '#collapsible' => TRUE,
    '#group' => 'oggchat',
  );

  $roles = user_roles();
  $role_options = array();
  foreach ($roles as $rid => $name) {
    $role_options[$rid] = $name;
  }
  $form['role_vis_settings']['oggchat_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Remove script for specific roles'),
    '#default_value' => variable_get('oggchat_roles', array()),
    '#options' => $role_options,
    '#description' => t('Remove script only for the selected role(s). If none of the roles are selected, all roles will have the script. Otherwise, any roles selected here will NOT have the script.'),
  );
  // END Role specific visibility configurations.

  // Page specific visibility configurations.
  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Pages'),
    '#collapsible' => TRUE,
    '#group' => 'oggchat',
  );

  $access = user_access('use PHP for oggchat visibility');
  $visibility = variable_get('oggchat_visibility', 0);
  $pages = variable_get('oggchat_pages', '');

  if ($visibility == 2 && !$access) {
    $form['page_vis_settings'] = array();
    $form['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(t('Add to every page except the listed pages.'), t('Add to the listed pages only.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
    if ($access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php tags. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['page_vis_settings']['oggchat_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Add script to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['page_vis_settings']['oggchat_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => $pages,
      '#description' => $description,
      '#wysiwyg' => FALSE,
    );
  }
  // END Page specific visibility configurations.

  return system_settings_form($form);
}

/**
 * Implementation of hook_admin_settings_form_validate().
 *
 * @param array $form
 *   structured associative drupal form array.
 * @param array $form_state
 */
function oggchat_admin_settings_form_validate($form, &$form_state) {
  if (empty($form_state['values']['oggchat_account'])) {
    form_set_error('oggchat_account', t('A valid OggChat chat widget key is needed.'));
  }
}
