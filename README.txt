### ABOUT
This module adds OggChat Live Chat widget to your website.  Just sign up for an OggChat trial account at www.oggchat.com 
and you're set.

Current Features:
 * Admin settings to configure your Chat Widget Key for your chat widget


### INSTALLING

  1. Extract OggChat Live Chat tarball into your sites/all/modules directory so it looks like sites/all/modules/oggchat
  2. Navigate to Administration -> Modules and enable the module.
  3. Navigate to Administration -> Configuration -> System -> OggChat and add your Chat Widget Key to the module.


